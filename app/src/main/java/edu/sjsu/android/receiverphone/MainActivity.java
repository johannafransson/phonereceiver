package edu.sjsu.android.receiverphone;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity<PERMISSION_REQUEST_CODE> extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(MainActivity.this, new String[]
                {android.Manifest.permission.READ_PHONE_STATE}, 100);
        ActivityCompat.requestPermissions(MainActivity.this, new String[]
                {Manifest.permission.READ_CALL_LOG}, 100);


    }
}